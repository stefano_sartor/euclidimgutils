/*
 * Copyright (C) 2012-2020 Euclid Science Ground Segment
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
#include <boost/python/copy_const_reference.hpp>
#include <boost/python/copy_non_const_reference.hpp>
#include "BoostBinding/py_eigen.h"
#include "ImageBase.h"
#include "Image.h"


template<typename PixelT>
void define_ImageBase(const char* class_name){
    using namespace boost::python;
    using namespace CommonTools;
    using namespace Euclid;

    typedef ImageBase<PixelT> image;

    const CommonTools::Array<PixelT>& (image::*const_get_array)() const= &image::getArray;

    class_<image>(class_name,init<int,int>())
      .def("getArray",const_get_array,return_value_policy<copy_const_reference>())
      .def("setArray",&image::setArray)
      .def("getWidth",&image::getWidth)
      .def("getHeight",&image::getHeight)
      ;
}


template<typename PixelT>
void define_Image(const char* class_name){
    using namespace boost::python;
    using namespace CommonTools;
    using namespace Euclid;
    
    typedef Image<PixelT> image;

    image& (image::*op_mul)(const PixelT& i) = &image::operator*=;
    image& (image::*op_mul_img)(const image& i) = &image::operator*=;
    const CommonTools::Array<PixelT>& (image::*const_get_array)() const= &image::getArray;
    
    class_<image>(class_name,init<int,int>())
      .def("__imul__",op_mul,return_value_policy<copy_non_const_reference>())
      .def("__imul__",op_mul_img,return_value_policy<copy_non_const_reference>())
      .def("getArray",const_get_array,return_value_policy<copy_const_reference>())
      ;
  
}



BOOST_PYTHON_MODULE(EuclidImagePyBoost){
    using namespace boost::python;
    using namespace CommonTools;
    using namespace Euclid;
    
    // initialize type maps for Eigen matrices
    EigenBindingInit();

    define_ImageBase<double>("ImageBaseDouble");
    define_Image<double>("ImageDouble");
    
}
