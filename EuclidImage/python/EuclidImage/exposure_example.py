import os                                 # for the path tools
import argparse                           # for program options from configuration
import ElementsKernel.Logging as log      # for Elements logging support

import numpy as np
from EuclidImageBinding import NispDetectorExposure as Exposure, MaskPlaneDict

def defineSpecificProgramOptions():
    """
    @brief Allows to define the (command line and configuration file) options specific to
    this program
    """                
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', type=str, dest="inputFile",
                        help='Input file')
    parser.add_argument('--hdu', type=int, dest="hdu", default = 2,
                        help='start hdu number')
    parser.add_argument('-o', type=str, dest="outFile",
                        help='Output file')    
    return parser

def mainMethod(args):
    """
    @brief The "main" method.
    @details
        This method is the entry point to the program. In this sense, it is similar to a main
        (and it is why it is called mainMethod()). The code below contains the calls to the
        different classes created for the first developer's workshop

        See the ElementsProgram documentation for more details. 
    """
    logger = log.getLogger('Exposure example')
    
   
    # This script has been tested by providing, as input file, both a SIR and a NIR exposure
    # produced by OU-SIM:
    #   * EUC-TEST-GRED180D11-2015-07-16T054123.848Z.fits
    #   * EUC-TEST-Y3-2015-07-15T081602.175Z.fits
    #
    # Example to use this script:
    #  > ./run Exposure_example -f EUC-TEST-GRED180D11-2015-07-16T054123.848Z.fits -o test.fits
    #  > ./run Exposure_example -f EUC-TEST-Y3-2015-07-15T081602.175Z.fits --hdu 1 -o test.fits
    
    detExposure = Exposure.readFits(args.inputFile, args.hdu, True)
    
    # by default, also the primary header is read in the metadata object (as a parent object)
    metadata = detExposure.getMetadata()
    
    # keys as searched by default also in the parent metadata object
    ra = metadata.get("RA_APER")
    dec = metadata.get("DEC_APER")
    
    
    print("RA: ", ra, "DEC: ", dec)
    
    # getting the three layers: science image, rms, and mask
    sciImage = detExposure.getImage()
    rmsImage = detExposure.getRms()
    maskImage = detExposure.getMask()
    
    # Bit plane definition. In future versions it should be read from the FITS file
    planeDict = MaskPlaneDict({"BAD":0, "SAT":1, "INTRP":2, "CR":3, "EDGE":4, "DETECTED":5})
    
    # Manually associating a bit plane definition to the mask
    maskImage.setMaskPlaneDict(planeDict)
    
    # compound assignment operator
    gain = 1.2
    sciImage *= gain
    
    x = 10 
    y = 75
    # To access a single pixel, use the python indexing 
    print( 'Pixel at ({},{}): {}'.format(x, y, sciImage[x,y]))
    
    # Image or Mask do not yet provide slicing. But you can access them as numpy arrays (see below)
    
    # gettting the numpy arrays from the image and the mask, Pixel memory buffers are shared
    # between each image and the corresponding array
    sciArray = sciImage.getArray()
    maskArray = maskImage.getArray()
    
    # when accessing pixels as numpy arrays, remember to invert the coordiantes
    # since the arrays are indexed by row and column
    print('Pixel at ({},{}): {}'.format(x, y, sciArray[y,x]))
    
    
    # using numpy to identify sarurated pixels. The following value is based on SIR
    # simulated data
    pixThreshold = 600000
    saturated = np.where(sciArray > pixThreshold)
    
    # setting the SAT flag to 1 for pixels above the theshold value
    maskArray[saturated] |= maskImage.getPlaneBitMask("SAT")
    
    # Adding some new metadata
    metadata.add("MEAN", sciArray.mean(), "Image mean value")

    # saving all three layers in a new FITS file
    detExposure.writeFits(args.outFile)
    
                               
    
    
