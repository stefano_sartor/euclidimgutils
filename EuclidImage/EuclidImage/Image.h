#ifndef EUCLID_IMG_UTILS_IMAGE_H
#define EUCLID_IMG_UTILS_IMAGE_H
/*
 * Copyright (C) 2012-2020 Euclid Science Ground Segment
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include "cfitsio/fitsio.h"
#include "ImageBase.h"

template<typename PixelT>
class PixelType{
public:
  static const int type;
  static const int read_type;
};

namespace Euclid{

class PropertyList;

template<typename PixelT>
class Image: public ImageBase<PixelT>{
public:
  using super = ImageBase<PixelT>;
  using typename super::size_type; 
  using typename super::Array;
  
  static Image<PixelT> readFits(const std::string& filename, int hdu = 1, PropertyList* metadata = nullptr);


  // construction from dimensions
  Image(size_type width, size_type height, PixelT initialValue = 0);
  // construction from another image
  Image(const Image& other, bool deepCopy = false);
  // construction from an array
  explicit Image(const Array& array, bool deepCopy = false);

  Image subImage(size_type x, size_type y,
      size_type width, size_type height) {
    if((x+width > super::getWidth()) || (y+height > super::getHeight()) ||
        x < 0 || y < 0)
      throw std::out_of_range("Requested sub-image is out of range.");

    Image output = Image(width, height);

    for (size_type cy = 0; cy < height; ++cy)
      for (size_type cx = 0; cx < width; ++cx)
        output(cx, cy) = this->operator()(x + cx, y + cy);

    return output;
  }

  void
  writeFits(const std::string&){  std::cout<<"not implemented";}

  virtual ~Image(){}


  Image& operator=(const PixelT& value);
  Image& operator=(const Image& other);
  
  Image& operator+=(const PixelT& rhs);
  Image& operator+=(const Image& rhs);

  Image& operator-=(const PixelT& rhs);
  Image& operator-=(const Image<PixelT>& rhs);

  Image& operator*=(const PixelT& rhs);
  Image& operator*=(const Image<PixelT> & rhs);

  Image& operator/=(const PixelT& rhs);
  Image& operator/=(const Image<PixelT>& rhs);

private:
  // Verifies if other has the same size as this Image
  void checkDimensions(const Image& other) const;

};

}

#endif
