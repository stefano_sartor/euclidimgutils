#ifndef EUCLID_IMG_UTILS_TFITS_H
#define EUCLID_IMG_UTILS_TFITS_H

/*
 * Copyright (C) 2012-2020 Euclid Science Ground Segment
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include "PropertyList.h"
#include "CTEigen/Array.h"
#include <exception>
#include <memory>

namespace Euclid{

class TFits {
public:
    /// @brief Return the file name associated with the FITS object or "<unknown>" if there is none.
    std::string getFileName() const;

    /// @brief Return the current HDU (1-indexed; 1 is the Primary HDU).
    int getHdu();

    /**
     *  @brief Set the current HDU.
     *
     *  @param[in] hdu                 The HDU to move to (1-indexed; 1 is the Primary HDU).
     *                                 The special value of 0 moves to the first extension
     *                                 if the Primary HDU is empty (has NAXIS==0) and the
     *                                 the Primary HDU is the current one.
     *  @param[in] relative            If true, move relative to the current HDU.
     */
    void setHdu(int hdu, bool relative=false);

    /// @brief Return the number of HDUs in the file.
    int countHdus();


    /**
     *  @brief Create an empty image HDU with NAXIS=0 at the end of the file.
     *
     *  This is primarily useful to force the first "real" HDU to be an extension HDU by creating
     *  an empty Primary HDU.  The new HDU is set as the active one.
     */
    void createEmpty();

    /// @brief Return the number of dimensions in the current HDU.
    int getImageDim();

    /**
     *  @brief Read a FITS header into a PropertyList.
     *
     *  @param[in]     metadata  A PropertySet or PropertyList whose items will be appended
     *                           to the FITS header.
     *
     *  All keys will be appended to the FITS header rather than used to update existing keys.
     *  Order of keys will be preserved.
     */
    void writeMetadata(const PropertyList& metadata,bool strip = true);

    /**
     *  @brief Read a FITS header into a PropertySet or PropertyList.
     *
     *  @param[in,out] metadata  A PropertySet or PropertyList that FITS header items will be added to.
     *  @param[in]     strip     If true, common FITS keys that usually have non-metadata intepretations
     *                           (e.g. NAXIS, BITPIX) will be ignored.
     *
     *  Order will preserved if and only if the metadata object is actually a PropertyList.
     */
    void readMetadata(PropertyList & metadata,bool strip = true);

    /**
     *  @brief Read an array from a FITS image.
     */
    template <typename PixelT>
    CommonTools::Array<PixelT> readImage();

    /**
     *  @brief Create a 2-d image with pixel type provided by the given explicit PixelT template parameter.
     *
     *  The new image will be in a new HDU at the end of the file, which may be the Primary HDU
     *  if the FITS file is empty.
     */
    template <typename PixelT>
    void createImage(long x, long y);

    /**
     *  @brief Write an CommotTools::Array to a FITS image HDU.
     *
     *  The HDU must already exist and have the correct bitpix.
     *
     */
    template <typename PixelT>
    void writeImage(const  CommonTools::Array<PixelT>& array);

    /**
     *  @brief Return true if the current HDU has the given pixel type..
     *
     *  This takes into account the BUNIT and BSCALE keywords, which can allow integer
     *  images to be interpreted as floating point.
     */
    template <typename T>
    bool checkImageType();

    TFits() : fptr(0), status(0) {}

    /// @brief Open or create a FITS file from disk.
    TFits(std::string const & filename, std::string const & mode);


    /// @brief Close a FITS file.
    void closeFile();

    ~TFits() { if (fptr) closeFile(); }
private:
    template <typename T>
    void writeKeyImpl(char const * key, T const & value, char const * comment);
    void writeKeyImpl(char const * key, std::string const & value, char const * comment);
    void writeKeyImpl(char const * key, bool const & value, char const * comment);
    void writeKeyImpl(char const * key, double const & value, char const * comment);

private:
    void * fptr;  // the actual cfitsio fitsfile pointer; void to avoid including fitsio.h here.
    int status;   // the cfitsio status indicator that gets passed to every cfitsio call.
};

class TFitsException: public std::runtime_error{
public:
    TFitsException(int status);
};

} // Euclid

#endif


