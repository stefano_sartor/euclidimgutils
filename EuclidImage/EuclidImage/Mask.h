#ifndef EUCLID_IMG_UTILS_MASK_H
#define EUCLID_IMG_UTILS_MASK_H
/*
 * Copyright (C) 2012-2020 Euclid Science Ground Segment
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include "ImageBase.h"
#include "CTEigen/Array.h"

#include <map>


namespace Euclid{

using MaskPlaneDict = std::map<std::string, int>;

template<typename MaskPixelT>
class Mask: public ImageBase<MaskPixelT> {
 public:
  
  using super = ImageBase<MaskPixelT>;
  using typename super::size_type;
  using Array = CommonTools::Array<MaskPixelT>;

  Mask(size_type width, size_type height, 
       const MaskPlaneDict& planeDefs = MaskPlaneDict());

  Mask(size_type width, size_type height,
       MaskPixelT initialValue,
       const MaskPlaneDict& planeDefs = MaskPlaneDict());

  Mask(const Mask& other, bool deepCopy = false);

  explicit Mask(const Array& other, bool deepCopy = false);

  Mask& operator=(const MaskPixelT& value);
  Mask& operator=(const Mask& other);

  Mask& operator|=(const MaskPixelT& value);
  Mask& operator|=(const Mask& other);

  Mask& operator&=(const MaskPixelT& value);
  Mask& operator&=(const Mask& other);

  Mask& operator^=(const MaskPixelT& value);
  Mask& operator^=(const Mask& other);

  using super::operator();
  using super::at;

  // Returns the bit value at position (x,y,plane)
  bool operator()(size_type x, size_type y, const std::string& plane) const;

  // Returns the bit value at position (x,y,plane) but check the position
  bool at(size_type x, size_type y, const std::string& plane) const;

  // Returns the plane number for the input plane name
  int getMaskPlane(const std::string& planeName) const;
  
  // Returns 1<< getMaskPlane(planeName)
  MaskPixelT getPlaneBitMask(const std::string& planeName) const;

  // Returns the Mask Plane Dictionary of the Mask
  const MaskPlaneDict& getMaskPlaneDict() const;
  // Sets the Mask Plane Dictionary of the Mask
  void setMaskPlaneDict(const MaskPlaneDict& planeDefs);

 private:

  // Verifies if other has the same size as this mask
  void checkDimensions(const Mask& other) const;

  // Returns 1 << plane
  static MaskPixelT getBitMask(int plane);
  MaskPlaneDict m_planeDefs;
};

/* inline methods definitions */

template<typename MaskPixelT>
inline MaskPixelT Mask<MaskPixelT>::getBitMask(int plane){
 return 1 << plane;
}

}// namespace Euclid



#endif
