
%module EuclidImageBinding

// Suppress swig complaints
#pragma SWIG nowarn=321  // swig warning on 'set', that is a built-in name in python

%{

#include "Image.h"
#include "ImageBase.h"
#include "Mask.h"
#include "PropertyList.h"
#include "DetectorExposure.h"
#include "SurveyExposure.h"

#include "swig/PropertyListIterator.hpp"
%}

// Generic standard exceptions handling
%include "exception.i"

%exception {
  try {
    $action
  } catch (const std::exception& e) {
    SWIG_exception(SWIG_RuntimeError, e.what());
  }
}


%include stl.i // We need this to use stl container
%include stdint.i  // to support uint8_t, uint32_t, etc.
%include std_string.i
%include std_vector.i
%include pyEigen.i

namespace std {
   %template(StringVector) vector<string>;
}

%template(MaskPlaneDict) std::map<std::string, int>;

namespace Euclid {
 
class PropertyList {
public:

    PropertyList();

    template<typename T>
    void set(const std::string& key, const T& e, const std::string& comment="", bool inPlace=true);
    template<typename T>
    void add(const std::string& key, const T& e, const std::string& comment="");

    void set(const std::string& key, const char* e, const std::string& comment="", bool inPlace=true);
    void add(const std::string& key, const char* e, const std::string& comment="");    

    // throws std::out_of_range if key is not present
    // throws boost::bad_any_cast if the template type argument is not compatible
    template<typename T>
    T get(const std::string& key);    
    
    // throws std::out_of_range if key is not present
    // throws boost::bad_any_cast if the underline type is not a bool
    bool getAsBool(const std::string& key);

    // throws std::out_of_range if key is not present
    // throws boost::bad_any_cast if the underline type is not lossless convertible to int
    int getAsInt(std::string const& key);

    // throws std::out_of_range if key is not present
    // throws boost::bad_any_cast if the underline type is not lossless convertible to int64_t
    int64_t getAsInt64(std::string const& key);

    // throws std::out_of_range if key is not present
    // throws boost::bad_any_cast if the underline type is not convertible to double
    double getAsDouble(std::string const& key);

    std::vector<std::string> keys();

    PropertyList makeChild();

    void setParent(const PropertyList& p);

    PropertyList getParent() const;

    bool hasParent()const;

    PropertyList copy()const;

    bool hasKey(const std::string& key, bool parent = true) const;

};

%define PropertyListAddType(Type)
%template(set) PropertyList::set<Type>;
%template(add) PropertyList::add<Type>;
%enddef

PropertyListAddType(bool)
PropertyListAddType(int)
PropertyListAddType(int64_t)
PropertyListAddType(double)


   
struct stop_iteration {};
 
%typemap(throws) stop_iteration %{
  (void)$1;
  SWIG_SetErrorObj(PyExc_StopIteration, SWIG_Py_Void());
  SWIG_fail;
  %}

class PropertyListIterator {
public:
  typedef PropertyList::iterator iterator;
  PropertyListIterator(iterator curr, iterator end);
  PyObject* next() throw(stop_iteration);
  PropertyListIterator&  __iter__();
};

 

%extend PropertyList {
  PyObject* get(const std::string& key, PyObject* defaultValue = nullptr) 
  {
    
    PyObject * result = nullptr;
    try {
      const std::type_info& t = self->typeOf(key);
      
      if (t == typeid(bool))               result =  PyBool_FromLong(self->get<bool>(key));
      if (t == typeid(char))               result =  PyInt_FromLong(self->get<char>(key));
      if (t == typeid(unsigned char))      result =  PyInt_FromLong(self->get<unsigned char>(key));
      if (t == typeid(short))              result =  PyInt_FromLong(self->get<short>(key));
      if (t == typeid(unsigned short))     result =  PyInt_FromLong(self->get<unsigned short>(key));
      if (t == typeid(int))                result =  PyInt_FromLong(self->get<int>(key));
      if (t == typeid(unsigned int))       result =  PyLong_FromUnsignedLong(self->get<unsigned int>(key));
      if (t == typeid(long))               result =  PyLong_FromLongLong(self->get<long>(key));
      if (t == typeid(unsigned long))      result =  PyLong_FromUnsignedLongLong(self->get<unsigned long>(key));
      if (t == typeid(long long))          result =  PyLong_FromLongLong(self->get<long long>(key));
      if (t == typeid(unsigned long long)) result =  PyLong_FromUnsignedLongLong(self->get<unsigned long long>(key));
      if (t == typeid(float))              result =  PyFloat_FromDouble(self->get<float>(key));    
      if (t == typeid(double))             result =  PyFloat_FromDouble(self->get<double>(key)); 
      if (t == typeid(std::string))
      {
        std::string val = self->get<std::string>(key);
        result =  PyString_FromStringAndSize(val.c_str(), val.size());
      }
    }
    catch (const std::out_of_range& e) {
      if (defaultValue)
        return defaultValue;
      else
        throw e;    
    }
    if (! result)
      throw std::runtime_error("Cannot convert key vlaue");
    return result;
  }


  Euclid::PropertyListIterator
  __iter__()
  {
    return Euclid::PropertyListIterator(self->begin(), self->end());
  }

  Euclid::PropertyListIterator
  iteritems()
  {
    return Euclid::PropertyListIterator(self->begin(), self->end());
  }  
  
  %pythoncode %{

  def items(self):
    l = []
    for i in self:
      l.append(i)
    return l

  def prova(self, key):
    return self.get(key)
    
  %}
  
  
}


template<typename PixelT>
class ImageBase{
 public:
  typedef int size_type;
  typedef PixelT value_type;
  typedef value_type& reference;
  typedef const value_type& const_reference;


  typedef CommonTools::Array<PixelT> Array;

  // constructor(s)
  ImageBase(size_type width, size_type height);

  ImageBase(const ImageBase& other, bool deepCopy = false);

  // from a CTEigen array
  explicit ImageBase(const Array& array, bool deepCopy = false);

  
  virtual ~ImageBase() {}


  // reference to a given array position
  reference operator() (size_type x, size_type y);

  // similar but check the indices
  reference at(size_type x, size_type y);

  // Returns the number of columns
  size_type getWidth() const;
  // Returns the number of rows
  size_type getHeight() const;

  Array getArray();

  void setArray(const Array array);

 };



template<typename PixelT>
class Image: public ImageBase<PixelT>{
public:
  typedef ImageBase<PixelT> super;
  typename super::size_type; 
  
  static Image<PixelT> readFits(const std::string& filename, int hdu = 1, PropertyList* metadata = nullptr);


  // construction from dimensions
  Image(size_type width, size_type height, PixelT initialValue = 0);
  // construction from another image
  Image(const Image& other, bool deepCopy = false);

  Image subImage(size_type x, size_type y,
                 size_type width, size_type height);


  virtual ~Image(){}

  
  Image& operator+=(const Image& rhs);
  Image& operator+=(const PixelT& rhs);

  Image& operator-=(const Image<PixelT>& rhs);
  Image& operator-=(const PixelT& rhs);

  Image& operator*=(const Image<PixelT> & rhs);
  Image& operator*=(const PixelT& rhs);

  Image& operator/=(const Image<PixelT>& rhs);
  Image& operator/=(const PixelT& rhs);

};


typedef std::map<std::string, int> MaskPlaneDict;

template<typename MaskPixelT>
class Mask: public ImageBase<MaskPixelT> {
 public:
  
  typedef ImageBase<MaskPixelT> super;
  typedef typename super::size_type;

  Mask(size_type width, size_type height, 
       const MaskPlaneDict& planeDefs = MaskPlaneDict());

  Mask(size_type width, size_type height,
       MaskPixelT initialValue,
       const MaskPlaneDict& planeDefs = MaskPlaneDict());

  Mask& operator|=(const MaskPixelT& value);
  Mask& operator|=(const Mask& other);

  Mask& operator&=(const MaskPixelT& value);
  Mask& operator&=(const Mask& other);

  Mask& operator^=(const MaskPixelT& value);
  Mask& operator^=(const Mask& other);
  
  // Returns the bit value at position (x,y,plane)
  bool operator()(size_type x, size_type y, const std::string& plane) const;
  // Returns the bit value at position (x,y,plane) but check the position
  bool at(size_type x, size_type y, const std::string& plane) const;

  // Returns the plane number for the input plane name
  int getMaskPlane(const std::string& planeName) const;
  
  // Returns 1<< getMaskPlane(planeName)
  MaskPixelT getPlaneBitMask(const std::string& planeName) const;

  // Returns the Mask Plane Dictionary of the Mask
  const MaskPlaneDict& getMaskPlaneDict() const;
  // Sets the Mask Plane Dictionary of the Mask
  void setMaskPlaneDict(const MaskPlaneDict& planeDefs);

};



template<typename ImageT, typename RootMeanSqT=Image<float>, typename MaskT=Mask<uint16_t>>
class DetectorExposure
{
public:
  typedef int size_type;

  DetectorExposure(size_type width, size_type height,
                   const MaskPlaneDict& planeDict = MaskPlaneDict());

  // For this prototype, we pass by value. TODO: improve by using shared pointers
  // Not a urgent issue since ImageBase uses a shared pointer to a raw buffer
  DetectorExposure(const ImageT& image, const RootMeanSqT& rms, const MaskT& mask);

  static DetectorExposure readFits(std::string fileName, int hdu = 2, bool primaryHdr = true);

  void writeFits(std::string fileName) const;

  ImageT& getImage() { return m_image;}

  // alias for getImage
  ImageT& getScience() { return m_image;}

  RootMeanSqT& getRms() { return m_rms;}

  MaskT& getMask() { return m_mask;}


  void setImage(const ImageT& image) { m_image = image;}
  // alias for setImage
  void setScience(const ImageT& image) { m_image = image;}

  void setRms(const RootMeanSqT& rms) { m_rms = rms;}

  void setMask(const MaskT& mask) { m_mask = mask;}
  
  PropertyList& getMetadata() { return m_metadata;}

  void setMetadata(const PropertyList& metadata) { m_metadata = metadata;}

  // TODO: Wcs implementation to be completed
  bool hasWcs() { return false;}

};

//class SurveyExposure{
//public:
//
//  SurveyExposure(){}
//
//  void setDetector(DetectorExposure& detector, short number);
//
//  DetectorExposure& getDetector(short number);
//
//};



%define %image(NAME, TYPE, PIXEL_TYPE...)
%template(NAME##TYPE##Base) ImageBase<PIXEL_TYPE>;
%template(NAME##TYPE) Image<PIXEL_TYPE>;    
%extend Image<PIXEL_TYPE> {
  void set(PIXEL_TYPE val)
  {
    *self = val;
  }
  
  void set(int x, int y, PIXEL_TYPE val)
  {
    self->at(x, y) = val;
  }
  
  PIXEL_TYPE get(int x, int y)
  {
    return self->at(x,y);
  }
  
  %pythoncode %{
    
  def __getitem__(self, pos):
    x, y = pos
    return self.get(x,y)
        
        
  def __setitem__(self, pos, val):
    x, y = pos
    self.set(x, y, val)
    
  %}  
  
}
%enddef

%define %mask(NAME, TYPE, PIXEL_TYPE...)
%template(NAME##TYPE) Mask<PIXEL_TYPE>;    
%extend Mask<PIXEL_TYPE> {
  void set(PIXEL_TYPE val)
  {
    *self = val;
  }
  
  void set(int x, int y, PIXEL_TYPE val)
  {
    self->at(x, y) = val;
  }
  
  PIXEL_TYPE get(int x, int y)
  {
    return self->at(x,y);
  }
  
  %pythoncode %{
    
  def __getitem__(self, pos):
    x, y = pos
    return self.get(x,y)
        
        
  def __setitem__(self, pos, val):
    x, y = pos
    self.set(x, y, val)
    
  %}  
  
}
%enddef


// To be checked if we need all these types, LSST provides instatiation for fewer types

%image(Image, S, short);
%image(Image, I, int);
%image(Image, L, long);
%image(Image, LL, long long);
%image(Image, F, float);
%image(Image, D, double);

%image(Image, UC, unsigned char);
%image(Image, US, unsigned short);
%image(Image, UI, unsigned int);
%image(Image, UL, unsigned long);
%image(Image, ULL, unsigned long long);

%mask(Mask, UC, unsigned char);
%mask(Mask, US, unsigned short);
%mask(Mask, UI, unsigned int);
%mask(Mask, UL, unsigned long);
%mask(Mask, ULL, unsigned long long);

%template(NispDetectorExposure) DetectorExposure<Image<float>>;

 
} // namespace Euclid

