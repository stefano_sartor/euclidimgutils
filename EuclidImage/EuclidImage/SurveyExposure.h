/*******************************************************************************
 * @file SurveyExposure.h
 *
 * Created on: 04, 01, 2015
 * @author INAF-IASF Milano
 *
 * @copyright Copyright (C) 2015-2020 Euclid Science Ground Segment
 *     
 * @licence This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)  
 * any later version.  
 *    
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied  
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more  
 * details.  
 *   
 * You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to  
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA  
 *     
 *******************************************************************************/
#ifndef SURVEYEXPOSURE_H
#define SURVEYREXPOSURE_H

#include "DetectorExposure.h"

#include <vector>

/**
 * Class representing survey exposure data.
 * DO NOT USE it yet. It is just a placeholder for future development
 */

namespace Euclid{


// TODO: definitely we need to use a vector of shared pointers here
class NispSurveyExposure{
public:

  NispSurveyExposure(){}

  void setDetector(NispDetectorExposure& detector, short number)
  {
    m_detectors[number] = detector;
  }

  NispDetectorExposure& getDetector(short number) {
    return(m_detectors[number]); }

private:
  std::vector<NispDetectorExposure> m_detectors;

};

}
#endif
