#ifndef EUCLID_IMG_UTILS_IMAGE_BASE_H
#define EUCLID_IMG_UTILS_IMAGE_BASE_H
/*
 * Copyright (C) 2012-2020 Euclid Science Ground Segment
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include "CTEigen/Array.h"

#include <iterator>

namespace Euclid{

template<typename PixelT>
class ImageBase{
 public:
  using size_type = int;
  using value_type = PixelT;
  using reference = value_type&;
  using const_reference = const value_type&;

  // Iterator as a simple pointer
  using iterator = value_type*;
  using const_iterator = const value_type*;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  using Array = CommonTools::Array<PixelT>;

  // constructor(s)
  ImageBase(size_type width, size_type height);

  ImageBase(const ImageBase& other, bool deepCopy = false);

  // from a CTEigen array
  explicit ImageBase(const Array& array, bool deepCopy = false);
  
  // TODO: conversion to another image type. Only deep copies are permitted
  // template<typename OtherPixelT>
  // ImageBase(const ImageBase<OtherPixelT>& other);

  virtual ~ImageBase() {}

  // Assighment operators
  ImageBase& operator=(const ImageBase& other);
  ImageBase& operator=(PixelT value);

  // Reference to a given array position
  reference operator() (size_type x, size_type y);
  const_reference operator() (size_type x, size_type y) const;

  // Similar but check the indices
  reference at(size_type x, size_type y);
  const_reference at(size_type x, size_type y) const;

  // Returns the number of columns
  size_type getWidth() const;
  // Returns the number of rows
  size_type getHeight() const;

  iterator begin() { return buffer_.data();}
  const_iterator begin() const { return buffer_.data();}
  const_iterator cbegin() const { return begin();}

  iterator end() { return buffer_.data() + (buffer_.cols()*buffer_.rows());}
  const_iterator end() const { return buffer_.data() + (buffer_.cols()*buffer_.rows());}
  const_iterator cend() const { return end();}
  
  reverse_iterator rbegin() { return reverse_iterator( end());}
  const_reverse_iterator rbegin() const {return const_reverse_iterator( end());}
  const_reverse_iterator crbegin() const { return rbegin();}

  reverse_iterator rend() { return reverse_iterator( begin());}
  const_reverse_iterator rend() const { return const_reverse_iterator( begin());}
  const_reverse_iterator crend() const { return rend();}

  Array& getArray();
  const Array& getArray() const;

  void setArray(const Array& array);

  //bool isContiguous() const {return begin() + getWidth()*getHeight() == end();}

 protected:

  CommonTools::Array<PixelT> buffer_;

 };
}
#endif
