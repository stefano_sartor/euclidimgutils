/*******************************************************************************
 * @file DetectorExposure.h
 *
 * Created on: 04, 01, 2015
 * @author INAF-IASF Milano
 *
 * @copyright Copyright (C) 2015-2020 Euclid Science Ground Segment
 *     
 * @licence This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)  
 * any later version.  
 *    
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied  
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more  
 * details.  
 *   
 * You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to  
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA  
 *     
 *******************************************************************************/
#ifndef DETECTOREXPOSURE_H
#define DETECTOREXPOSURE_H


#include "Image.h"
#include "Mask.h"
#include "PropertyList.h"

#include <string>

/**
 * Class representing detector-related data.
 */

namespace Euclid{



template<typename ImageT, typename RootMeanSqT=Image<float>, typename MaskT=Mask<uint16_t>>
class DetectorExposure
{
public:
  using size_type = int;


  DetectorExposure(size_type width, size_type height,
                   const MaskPlaneDict& planeDict = MaskPlaneDict());

  // For this prototype, we pass by value. TODO: improve by using shared pointers
  // Not a urgent issue since ImageBase uses a shared pointer to a raw buffer
  DetectorExposure(const ImageT& image, const RootMeanSqT& rms, const MaskT& mask);

  DetectorExposure(const DetectorExposure& other, bool deepCopy = false);

  static DetectorExposure readFits(std::string fileName, int startHdu = 2, bool primaryHdr = true);

  void writeFits(std::string fileName) const;

  ImageT& getImage() { return m_image;}
  const ImageT& getImage() const { return m_image;}

  // alias for getImage
  ImageT& getScience() { return m_image;}
  const ImageT& getScience() const { return m_image;}

  RootMeanSqT& getRms() { return m_rms;}
  const RootMeanSqT& getRms() const { return m_rms;}

  MaskT& getMask() { return m_mask;}
  const MaskT& getMask()const { return m_mask;}



  void setImage(const ImageT& image) { m_image = image;}
  // alias for setImage
  void setScience(const ImageT& image) { m_image = image;}

  void setRms(const RootMeanSqT& rms) { m_rms = rms;}

  void setMask(const MaskT& mask) { m_mask = mask;}


  PropertyList& getMetadata() { return m_metadata;}
  const PropertyList& getMetadata() const {return m_metadata;}

  void setMetadata(const PropertyList& metadata) { m_metadata = metadata;}

  // TODO: Wcs implementation to be completed
  bool hasWcs() { return false;}


private:

  void conformSizes();

  ImageT m_image;
  RootMeanSqT m_rms;
  MaskT m_mask;


  PropertyList m_metadata;

};


using NispDetectorExposure = DetectorExposure<Image<float>>;




}
#endif
