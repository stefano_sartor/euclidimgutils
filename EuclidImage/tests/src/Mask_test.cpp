/**
 * @file Mask_test.cpp
 *
 * @date Jul 21, 2015
 * @author M. Frailis
 */


#define BOOST_TEST_MODULE Mask_test
#include <boost/test/unit_test.hpp>

#include "Mask.h"
#include "CTEigen/Array.h"

using namespace Euclid;
using CommonTools::Array;


const MaskPlaneDict MASK_DICT_PROJ1 = {{"BAD", 0}, {"SAT",1}, {"INTRP", 2},
                                       {"CR", 3}, {"EDGE", 4}, {"DETECTED", 5}};

const MaskPlaneDict MASK_DICT_PROJ2 = {{"BAD", 0}, {"CTI", 1}, {"SAT",2}, {"INTRP", 3},
                                       {"CR", 4}, {"EDGE", 5}, {"DETECTED", 6}};


BOOST_AUTO_TEST_SUITE (Mask_test)

BOOST_AUTO_TEST_CASE( constructors_test )
{

  int size = 2048;

  Mask<uint16_t> mask1(size, size, 0, MASK_DICT_PROJ1);

  mask1(10, 15) |= mask1.getPlaneBitMask("CR");

  // TODO: Maybe a copy() method would be more intuitive than a constructor with a deepCopy parameter
  Mask<uint16_t> mask2(mask1, true);

  BOOST_CHECK(mask1(10,15) == mask2(10,15) && mask1.getMaskPlaneDict() == mask2.getMaskPlaneDict() &&
              mask1.getArray().data() != mask2.getArray().data());

  Mask<uint16_t> mask3(2*size, 2*size, 1);

  mask3 = mask2;

  BOOST_CHECK(mask3.getWidth() == size && mask3.getHeight() == mask2.getHeight() &&
              mask3.getMaskPlaneDict() == mask2.getMaskPlaneDict());

  Array<uint32_t> flags(size, size);
  flags.fill(0x40);  // setting bit "DETECTED"

  Mask<uint32_t> mask4(flags);
  BOOST_CHECK(mask4.getArray().data() == flags.data());

  Mask<uint32_t> mask5(flags, true);
  BOOST_CHECK(mask5.getArray().data() != flags.data() &&
              mask5.getArray().matrix() == flags.matrix());

}


BOOST_AUTO_TEST_CASE( operators_test )
{

  int size = 2048;

  Mask<uint64_t> mask1(size, size, 3, MASK_DICT_PROJ1);

  Mask<uint64_t> mask2(size, size, 1 << 4, MASK_DICT_PROJ1);

  mask1 |= mask2;
  mask1 |= mask1.getPlaneBitMask("INTRP");

  BOOST_CHECK(mask1.getArray().isConstant(23));

  mask1 &= mask2;

  BOOST_CHECK(mask1.getArray().isConstant(16));

  mask1 &= 17;

  BOOST_CHECK(mask1.getArray().isConstant(16));

  mask2.getArray().block(0, 0, 1024, 1024).fill(17);

  mask1 ^= mask2;

  BOOST_CHECK(mask1.getArray().block(0, 0, 1024, 1024).isConstant(1));

  mask1 ^= 1 << 4;

  BOOST_CHECK(mask1.getArray().block(0, 0, 1024, 1024).isConstant(17));

  BOOST_CHECK(mask1(33,15, "EDGE"));
  BOOST_CHECK(mask1(33,15, "BAD"));
  BOOST_CHECK(!mask1(33,15, "CR"));


}


BOOST_AUTO_TEST_CASE( methods_test )
{

  int size = 2048;

  Mask<uint16_t> mask1(size, size, MASK_DICT_PROJ1);

  BOOST_CHECK(mask1.getMaskPlane("DETECTED") == 5 &&
              mask1.getMaskPlane("BAD") == 0);

  BOOST_CHECK(mask1.getPlaneBitMask("DETECTED") == (1 << 5) &&
              mask1.getPlaneBitMask("BAD") == 1);

  mask1.setMaskPlaneDict(MASK_DICT_PROJ2);

  BOOST_CHECK(mask1.getPlaneBitMask("CTI") == 2 &&
              mask1.getPlaneBitMask("SAT") == 4);

  mask1 |= (mask1.getPlaneBitMask("BAD") | mask1.getPlaneBitMask("CTI"));
  BOOST_CHECK(mask1.getArray().isConstant(3));


}


BOOST_AUTO_TEST_CASE( exceptions_test )
{

  int size = 2048;

  Mask<uint16_t> mask1(size, size, MASK_DICT_PROJ1);

  Mask<uint16_t> mask2(2*size, 2*size, 1, MASK_DICT_PROJ2);

  // Not checking if they have different dictionaries yet
  BOOST_CHECK_THROW(mask1 |= mask2, std::length_error);
  BOOST_CHECK_THROW(mask1 &= mask2, std::length_error);
  BOOST_CHECK_THROW(mask1 ^= mask2, std::length_error);
  BOOST_CHECK_THROW(mask1.at(2048,2048), std::out_of_range);

}


BOOST_AUTO_TEST_SUITE_END ()

