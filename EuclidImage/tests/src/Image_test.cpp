/**
 * @file Image_test.cpp
 *
 * @date Jul 19, 2015
 * @author M. Frailis
 */

#define BOOST_TEST_MODULE Image_test
#include <boost/test/unit_test.hpp>

#include "Image.h"
#include "CTEigen/Array.h"

using namespace Euclid;
using CommonTools::Array;

BOOST_AUTO_TEST_SUITE (Image_test)

BOOST_AUTO_TEST_CASE( constructors_test )
{

  int width = 200, height = 300;

  Image<double> img1(width, height);

  BOOST_CHECK(img1.getWidth()*img1.getHeight() == width*height);

  double sum = 0.0;
  for (const auto& x: img1)
    sum += x;
  BOOST_CHECK_CLOSE(sum, 0.0, 1E-14);

  double value = 5;
  Image<double> img2(width, height, value);
  for (const auto& x: img2)
    sum += x;
  BOOST_CHECK_CLOSE(sum, 5*width*height, 1E-14);

  auto img3 = img2;
  BOOST_CHECK(img3.getArray().data() == img2.getArray().data());

  Image<double> img4(img3, true);
  BOOST_CHECK(img4.getArray().data() != img3.getArray().data());
  BOOST_CHECK(img4.getArray().matrix() == img3.getArray().matrix());

  Array<float> array(width, height);
  array.setRandom();
  Image<float> img5(array);
  BOOST_CHECK(img5.getArray().data() == array.data());

  Image<float> img6(array, true);
  BOOST_CHECK(img6.getArray().data() != array.data());
}

#include <iostream>

BOOST_AUTO_TEST_CASE( operators_test )
{

  int width = 800, height = 600;

  Image<float> img1(width, height, 1.0);
  Image<float> img2(height, width, 2.0);

  img1 = img2;
  BOOST_CHECK(img1.getWidth() == height && img1.getHeight() == width &&
              img1.getArray().data() == img2.getArray().data());

  // incrementing even elements
  for (auto i = img1.begin(); i != img1.end(); i += 2)
    *i += 2;

  // multiplying odd elements
  Image<float>::size_type x, y;
  for (y = 0; y < img1.getHeight(); ++y)
    for(x = 1; x < img1.getWidth(); x += 2)
      img1(x, y) *= 2;


  BOOST_CHECK(img1.getArray().isConstant(4.0));

  Image<float> img3(height, width, 31.68);

  img1 += 25.2;
  img1 -= 5.2;
  img1 *= 3.3;
  img1 /= 2.5;

  BOOST_CHECK(img1.getArray().isConstant(31.68));

  img1 = 1.0;

  img1 += img3;
  img1 -= img3;
  img1 /= img3;
  img1 *= img3;

  BOOST_CHECK(img1.getArray().isConstant(1.0));

  Image<float> img4(10, 10, 2.0);
  BOOST_CHECK_THROW(img1 += img4, std::length_error);
  BOOST_CHECK_THROW(img1 -= img4, std::length_error);
  BOOST_CHECK_THROW(img1 *= img4, std::length_error);
  BOOST_CHECK_THROW(img1 /= img4, std::length_error);

}



BOOST_AUTO_TEST_SUITE_END ()

