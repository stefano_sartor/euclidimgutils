#include "TFits.h"
#include <iostream>

using namespace CommonTools;
using namespace Euclid;

int main(){
    std::string filename = "test_image.fits";

    Array<unsigned short> a(7,9);
    a << -1,-10000,-20000,-30000,-40000;


    a(0,a.cols()-1)=-1;
    a(a.rows()-1,a.cols()-1)=5000;
    {
        TFits fits(filename,"w");

        fits.createImage<unsigned short>(9,7);

        fits.writeImage(a);
    }

    Array<float> b(0,0);


        TFits fits(filename,"r");


        b = fits.readImage<float>();


    std::cout << a << std::endl;
    std::cout << "+---+---+---+---+---+---+---+---+---+---+" << std::endl;
    std::cout << b << std::endl;
    std::cout << "image pixel float ? "<<std::boolalpha<<  fits.checkImageType<float>()<<std::endl;
    std::cout << "image pixel unsigned short? " <<  fits.checkImageType<unsigned short>()<<std::endl;
    return 0;
}

