#include "Image.h"
#include "Mask.h"
#include <iostream>

using namespace Euclid;
using namespace std;


int main(){
  Image<float> i(5,3);
  i.getArray().setOnes();

  Image<float> i2(5,3);
  i2.getArray().setOnes();


  cout << "i" << endl;
  cout << i.getArray() << endl;

  cout << "i2" << endl;
  cout << i2.getArray() << endl;

  i += i2; 
  cout << "i" << endl;
  cout << i.getArray() << endl;

  i += i; 
  cout << "i" << endl;

  i(0,0) = 0;
  i(0,1) = 1;  
  cout << i.getArray() << endl;
  cout << "w=" << i.getWidth() << endl;
  cout << "h=" << i.getHeight() << endl;

  return 0;
}
