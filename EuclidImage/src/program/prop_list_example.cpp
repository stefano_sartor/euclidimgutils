#include "PropertyList.h"
#include <iostream>

using namespace Euclid;

int main(){

    PropertyList p;
    p.add("foo",5,"this is a comment");
    p.add("bar",8,"this is a useless comment");
    p.add("key3",10,"TBD comment");

    p.print();
    p.set("bar",1.55,"");
    std::cout << "-----" << std::endl;
    p.print();


    p.set("bar",50,"set to bottom",false);
    std::cout << "-----" << std::endl;
    p.print();


    std::cout << "+++++" << std::endl<< std::endl;

    PropertyList p1= p.makeChild();
    PropertyList p2= p.makeChild();


    p1.add("foo",0,"child key");
    p1.add("bar",0,"child key");

    p2.add("foo",0,"child key2");
    p2.add("bar",0,"child key2");

    p1.set("key3",11,"set from child");

    p1.set("keyword4",22,"add key");


    p1.print();
    std::cout << std::endl << std::endl;
    p2.print();

    std::cout << std::endl << std::endl;

    PropertyList p21= p2.makeChild();

    p21.add("key5","this is the key 5");
    p21.print();
    p21.add("key5",55);


    p21.print();




  return 0;
}
