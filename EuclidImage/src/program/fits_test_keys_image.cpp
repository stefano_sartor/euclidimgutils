#include "PropertyList.h"
#include "TFits.h"

#include <iostream>
//#include <pair>
//#include <vector>

using namespace Euclid;
using namespace CommonTools;

int main(int argc, char **argv){
//    using Hdu = std::pair<PropertyList,Array<float>>;
//    std::vector<Hdu> data;

    if(argc!=3)
        return 1;

    std::string infile = argv[1];
    std::string outfile = argv[2];

    TFits in (argv[1],"r");
    TFits out(argv[2],"w");

    for(int i=1; i<= in.countHdus(); i++){
        in.setHdu(i);
        PropertyList p;
        in.readMetadata(p);
        if(in.getImageDim()==2){ // copy only images
            Array<float> a = in.readImage<float>();
           out.createImage<float>(a.cols(),a.rows());
           out.writeImage(a);
        }else{
            out.createEmpty();
        }
        out.writeMetadata(p);
    }

    return 0;

}
