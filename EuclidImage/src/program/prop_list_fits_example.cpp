#include "PropertyList.h"
#include "TFits.h"

#include <iostream>

using namespace Euclid;

int main(){
    std::string filename = "test_keywords.fits";

    bool b = false;
    char c = 67;
    unsigned char uc = 9;
    short s = -270;
    unsigned short us= 350;
    int i = -1560;
    unsigned int ui = 23444;
    long long ll = -(1LL<<31);
    unsigned long long ull = (1LL<<31);
//    int64_t i64 = -(1LL<<31);
//    uint64_t ui64 = (1LL<<31);
    float f = 2344.49230;
    double d= 2345234.35;
    std::string str = "this is a cool string";
    const char * cs= "C string";

    {
        PropertyList p;

        p.add("SIMPLE",true,"conforms to FITS standard");
        p.add("BITPIX",8,"array data type");
        p.add("NAXIS",0,"number of array dimensions");

        p.add("my_bool_key",b);
        p.add("my_char_key",c);
        p.add("my_uchar_key",uc);
        p.add("my_short_key",s);
        p.add("my_ushort_key",us);
        p.add("my_int_key",i);
        p.add("my_uint_key",ui);
        p.add("my_longlong_key",ll);
        p.add("my_ulonglong_key",ull);
//        p.add("my_i64_key",i64);
//        p.add("my_ui64_key",ui64);
        p.add("my_float_key",f);
        p.add("my_double_key",d);
        p.add("my_string_key",str);
        p.add("my_c_string_key",cs);

        p.print();

        TFits fits(filename,"w");

        fits.writeMetadata(p);
    }

    std::cout << "+---+---+---+---+---+---+---+---+---+---+" << std::endl;

    {
        TFits fits(filename,"r");
        PropertyList p;

        fits.readMetadata(p);
        p.print();

    }

    return 0;
}
