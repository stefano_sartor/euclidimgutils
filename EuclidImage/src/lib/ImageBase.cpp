/*
 * Copyright (C) 2012-2020 Euclid Science Ground Segment
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <algorithm>
#include "CTEigen/Array.h"
#include "ImageBase.h"

#include <sstream>

namespace Euclid{

template<typename PixelT> ImageBase<PixelT>::ImageBase(size_type width, size_type height) :
  buffer_(height,width)
{}

template<typename PixelT> 
ImageBase<PixelT>::ImageBase(const ImageBase<PixelT>& other, bool deepCopy):
  buffer_(0,0)
{
  // to be improved, using the initialization list
  if (!deepCopy) {
    buffer_ = other.getArray();
  } else {
    buffer_ = other.getArray().copy();
  }
}


template<typename PixelT> 
ImageBase<PixelT>::ImageBase(const Array& array, bool deepCopy) :
  buffer_(0,0)
{
  if (!deepCopy) {
    buffer_ = array;
  } else {
    buffer_= array.copy();
  }
}



template<typename PixelT> 
ImageBase<PixelT>& ImageBase<PixelT>::operator=(const ImageBase<PixelT>& rhs) {
  if (this == &rhs)
    return *this;

  this->buffer_ = rhs.buffer_;
  return *this;
}


template<typename PixelT> 
ImageBase<PixelT>& ImageBase<PixelT>::operator=(const PixelT rhs) {
  buffer_.fill(rhs);
  return *this;
}

template<typename PixelT> 
int ImageBase<PixelT>::getWidth() const {
  return buffer_.cols();
}

template<typename PixelT> 
int ImageBase<PixelT>::getHeight() const {
  return buffer_.rows();
}

template<typename PixelT> 
PixelT& ImageBase<PixelT>::operator()(size_type x, size_type y) {
  return buffer_(y,x);
}

template<typename PixelT> 
const PixelT& ImageBase<PixelT>::operator()(size_type x, size_type y) const {
  return buffer_(y,x);
}

template<typename PixelT> 
PixelT& ImageBase<PixelT>::at(size_type x, size_type y){
  // check dimension
  if ( (x < 0) || (x >= getWidth()) || (y<0) || (y>=getHeight())) {
    std::ostringstream what;
    what << "Indexes (" << x << ", " << y << ") are out of bounds";
    throw std::out_of_range(what.str());
  }
  return buffer_(y,x);
}

template <typename PixelT> 
const PixelT& ImageBase<PixelT>::at(size_type x, size_type y) const {
  // check dimension
  if ( (x<0) || (x>= getWidth()) || (y<0) || (y>=getHeight())) {
    std::ostringstream what;
    what << "Indexes (" << x << ", " << y << ") are out of bounds";
    throw std::out_of_range(what.str());
  }
  return buffer_(y,x);
}

template <typename PixelT> 
CommonTools::Array<PixelT>& ImageBase<PixelT>::getArray() {
  return buffer_;
}

template <typename PixelT> 
const CommonTools::Array<PixelT>& ImageBase<PixelT>::getArray() const {
  return buffer_;
}

template <typename PixelT> 
void ImageBase<PixelT>::setArray(const Array& array) {
  buffer_ = array;
}


#define INSTANTIATE(T) \
  template class ImageBase<T>


INSTANTIATE(short);
INSTANTIATE(int);
INSTANTIATE(long);
INSTANTIATE(long long);
INSTANTIATE(float);
INSTANTIATE(double);

INSTANTIATE(unsigned char);
INSTANTIATE(unsigned short);
INSTANTIATE(unsigned int);
INSTANTIATE(unsigned long);
INSTANTIATE(unsigned long long);

}

