/*
 * Copyright (C) 2012-2020 Euclid Science Ground Segment
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
#include "Image.h"
#include "PropertyList.h"
#include "TFits.h"

#include <stdexcept>
#include <boost/format.hpp>

namespace Euclid {

template<typename PixelT>
Image<PixelT>::Image(size_type width,size_type height, PixelT initialValue) :
  super(width,height)
{
  *this = initialValue;
}
  

template<typename PixelT>
Image<PixelT>::Image(const Image& rhs, bool deepCopy) : 
  super(rhs,deepCopy) {}


template<typename PixelT>
Image<PixelT>::Image(const Array& array, bool deepCopy) :
  super(array,deepCopy) {}


template<typename PixelT>
Image<PixelT> Image<PixelT>::readFits(const std::string& fileName, int hdu, PropertyList* metadata)
{
  TFits inFits(fileName, "r");
  int hduCount = inFits.countHdus();

  if (hdu > hduCount)
    throw std::runtime_error(str(boost::format("Invalid HDU number: %d. File has %d HDUs") %
        hdu % hduCount));

  inFits.setHdu(hdu);
  if (metadata)
    inFits.readMetadata(*metadata);

  return Image(inFits.readImage<PixelT>());

}


template<typename PixelT>
Image<PixelT>& Image<PixelT>::operator=(const Image<PixelT>& rhs)
{
  super::operator=(rhs);
  return *this;
}


template<typename PixelT>
Image<PixelT>& Image<PixelT>::operator=(const PixelT& rhs)
{
  super::operator=(rhs);
  return *this;
}


template<typename PixelT>
Image<PixelT>& Image<PixelT>::operator+=(const Image& rhs)
{
  checkDimensions(rhs);
  this->buffer_ += rhs.buffer_;
  return *this;
}


template<typename PixelT>
Image<PixelT>& Image<PixelT>::operator+=(const PixelT& rhs) {
  this->buffer_ += rhs;
  return *this;
}


template<typename PixelT>
Image<PixelT>& Image<PixelT>::operator-=(const Image& rhs)
{
  checkDimensions(rhs);
  this->buffer_ -= rhs.buffer_;
  return *this;
}

template<typename PixelT>
Image<PixelT>& Image<PixelT>::operator-=(const PixelT& rhs) {
  this->buffer_ -= rhs;
  return *this;
}


template<typename PixelT>
Image<PixelT>& Image<PixelT>::operator*=(const Image& rhs)
{
  checkDimensions(rhs);
  this->buffer_ *= rhs.buffer_;
  return *this;
}


template<typename PixelT>
Image<PixelT>& Image<PixelT>::operator*=(const PixelT& rhs) {
  this->buffer_ *= rhs;
  return *this;
}

template<typename PixelT>
Image<PixelT>& Image<PixelT>::operator/=(const Image& rhs)
{
  checkDimensions(rhs);
  this->buffer_ /= rhs.buffer_;
  return *this;
}


template<typename PixelT>
Image<PixelT>& Image<PixelT>::operator/=(const PixelT& rhs) {
  this->buffer_ /= rhs;
  return *this;
}


template<typename PixelT>
void Image<PixelT>::checkDimensions(const Image<PixelT>& other) const
{
  if (this->getWidth() != other.getWidth() || this->getHeight() != other.getHeight())
    throw std::length_error(str(boost::format("Images are of different size: %dx%d v %dv%d") %
                                          this->getWidth() % this->getHeight() %
                                          other.getWidth() % other.getHeight()));
}


#define INSTANTIATE(T) \
  template class Image<T>


INSTANTIATE(short);
INSTANTIATE(int);
INSTANTIATE(long);
INSTANTIATE(long long);
INSTANTIATE(float);
INSTANTIATE(double);

INSTANTIATE(unsigned char);
INSTANTIATE(unsigned short);
INSTANTIATE(unsigned int);
INSTANTIATE(unsigned long);
INSTANTIATE(unsigned long long);

}
 

