/*
 * Copyright (C) 2012-2020 Euclid Science Ground Segment
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include "TFits.h"

#include <string>
#include <iostream>
#include <sstream>
#include <cmath>
#include <limits.h>

#include <boost/filesystem.hpp>
#include <boost/regex.hpp>
#include <boost/preprocessor/seq/for_each.hpp>


extern "C" {
#include <fitsio.h>
}


namespace Euclid{

// ---- Traits class to get cfitsio type constants from templates -------------------------------------------

template <typename T> struct FitsType;


template <> struct FitsType<bool> { static int const CONSTANT = TLOGICAL; };
template <> struct FitsType<char> { static int const CONSTANT = TSTRING; };
template <> struct FitsType<unsigned char> { static int const CONSTANT = TBYTE; };
template <> struct FitsType<short> { static int const CONSTANT = TSHORT; };
template <> struct FitsType<unsigned short> { static int const CONSTANT = TUSHORT; };
template <> struct FitsType<int> { static int const CONSTANT = TINT; };
template <> struct FitsType<unsigned int> { static int const CONSTANT = TUINT; };
template <> struct FitsType<long> { static int const CONSTANT = TLONG; };
template <> struct FitsType<unsigned long> { static int const CONSTANT = TULONG; };
template <> struct FitsType<long long> { static int const CONSTANT = TLONGLONG; };
template <> struct FitsType<unsigned long long> { static int const CONSTANT = TLONGLONG; };
template <> struct FitsType<float> { static int const CONSTANT = TFLOAT; };
template <> struct FitsType<double> { static int const CONSTANT = TDOUBLE; };
template <> struct FitsType< std::complex<float> > { static int const CONSTANT = TCOMPLEX; };
template <> struct FitsType< std::complex<double> > { static int const CONSTANT = TDBLCOMPLEX; };

// We use TBIT when writing booleans to table cells, but TLOGICAL in headers.
template <typename T> struct FitsTableType : public FitsType<T> {};
template <> struct FitsTableType<bool> { static int const CONSTANT = TBIT; };

template <typename T> struct FitsBitPix;


template <> struct FitsBitPix<unsigned char> { static int const CONSTANT = BYTE_IMG; };
template <> struct FitsBitPix<short> { static int const CONSTANT = SHORT_IMG; };
template <> struct FitsBitPix<unsigned short> { static int const CONSTANT = USHORT_IMG; };
template <> struct FitsBitPix<int> { static int const CONSTANT = LONG_IMG; }; // not a typo!
template <> struct FitsBitPix<unsigned int> { static int const CONSTANT = ULONG_IMG; };
template <> struct FitsBitPix<long> { static int const CONSTANT =
      (std::numeric_limits<long>::digits == 63) ? LONGLONG_IMG : LONG_IMG; };
template <> struct FitsBitPix<unsigned long> { static int const CONSTANT =
      (std::numeric_limits<unsigned long>::digits == 64) ? LONGLONG_IMG : ULONG_IMG; };
template <> struct FitsBitPix<long long> { static int const CONSTANT = LONGLONG_IMG; };
template <> struct FitsBitPix<unsigned long long> { static int const CONSTANT = LONGLONG_IMG; };
template <> struct FitsBitPix<float> { static int const CONSTANT = FLOAT_IMG; };
template <> struct FitsBitPix<double> { static int const CONSTANT = DOUBLE_IMG; };



static std::string strip(std::string const & s);
static double stringToNonFiniteDouble(std::string const& value);
static std::string nonFiniteDoubleToString(double value);
static bool isKeyIgnored(std::string const & key);
static void loadKey(PropertyList&,const std::string&,const std::string&,const std::string&,bool);


static std::string fits_error_string(int status){
    char msg[FLEN_ERRMSG];
    fits_get_errstatus(status, msg);
    return std::string(msg);
}

TFitsException::TFitsException(int status): std::runtime_error(fits_error_string(status)){}

std::string TFits::getFileName() const {
    std::string fileName = "<unknown>";
    fitsfile * fd = reinterpret_cast<fitsfile*>(fptr);
    if (fd != 0 && fd->Fptr != 0 && fd->Fptr->filename != 0) {
        fileName = fd->Fptr->filename;
    }
    return fileName;
}


int TFits::getHdu() {
    int n = 0;
    fits_get_hdu_num(reinterpret_cast<fitsfile*>(fptr), &n);
    return n;
}


int TFits::getImageDim() {
    int nAxis = 0;
    fits_get_img_dim(reinterpret_cast<fitsfile*>(fptr), &nAxis, &status);
    if(status) throw TFitsException(status);
    return nAxis;
}


void TFits::setHdu(int hdu, bool relative) {
    if (relative) {
        fits_movrel_hdu(reinterpret_cast<fitsfile*>(fptr), hdu, 0, &status);
    } else {
        if (hdu != 0) {
            fits_movabs_hdu(reinterpret_cast<fitsfile*>(fptr), hdu, 0, &status);
        }
        if (hdu == 0 && getHdu() == 1 && getImageDim() == 0) {
            // want a silent failure here
            int tmpStatus = status;
            fits_movrel_hdu(reinterpret_cast<fitsfile*>(fptr), 1, 0, &tmpStatus);
        }
    }
    if(status) throw TFitsException(status);
}

int TFits::countHdus() {
    int n = 0;
    fits_get_num_hdus(reinterpret_cast<fitsfile*>(fptr), &n, &status);
    if(status) throw TFitsException(status);
    return n;
}


TFits::TFits(std::string const & filename, std::string const & mode)
    : fptr(0), status(0)
{
    if (mode == "r" || mode == "rb") {
        fits_open_file(
            reinterpret_cast<fitsfile**>(&fptr),
            const_cast<char*>(filename.c_str()),
            READONLY,
            &status
        );
    } else if (mode == "w" || mode == "wb") {
        boost::filesystem::remove(filename); // cfitsio doesn't like over-writing files
        fits_create_file(
            reinterpret_cast<fitsfile**>(&fptr),
            const_cast<char*>(filename.c_str()),
            &status
        );
    } else if (mode == "a" || mode == "ab") {
        fits_open_file(
            reinterpret_cast<fitsfile**>(&fptr),
            const_cast<char*>(filename.c_str()),
            READWRITE,
            &status
        );
        int nHdu = 0;
        fits_get_num_hdus(reinterpret_cast<fitsfile*>(fptr), &nHdu, &status);
        fits_movabs_hdu(reinterpret_cast<fitsfile*>(fptr), nHdu, NULL, &status);
        if (status) {
            // We're about to throw an exception, and the destructor won't get called
            // because we're in the constructor, so cleanup here first.
            int tmpStatus = 0;
            fits_close_file(reinterpret_cast<fitsfile*>(fptr), &tmpStatus);
            fptr=nullptr;
            throw TFitsException(status);
        }
    } else {
        std::ostringstream ss;
        ss<<"Invalid mode '"<<mode<<"' given when opening file '"<<filename<<"'.";
        throw std::invalid_argument(ss.str());
    }
}

void TFits::closeFile() {
    if (fptr){
        fits_close_file(reinterpret_cast<fitsfile*>(fptr), &status);
        fptr=nullptr;
    }
}



void TFits::readMetadata(PropertyList& l,bool is_strip) {
    char key[FLEN_CARD];                       // allow for terminating NUL
    char value[FLEN_CARD];
    char comment[FLEN_CARD];

    int nKeys = 0;
    fits_get_hdrspace(reinterpret_cast<fitsfile*>(fptr), &nKeys, 0, &status);
    if(status){// TODO handle properly
      throw TFitsException(status);
    }
    std::string keyStr;
    std::string valueStr;
    std::string commentStr;
    int i = 1;
    while (i <= nKeys) {
        fits_read_keyn(reinterpret_cast<fitsfile*>(fptr), i, key, value, comment, &status);
        if(status)
          throw TFitsException(status);

        keyStr = key;
        valueStr = value;
        commentStr = comment;
        ++i;
        while (valueStr.size() > 2 && valueStr[valueStr.size() - 2] == '&' && i <= nKeys) {
            // we're using key to hold the entire record here; the actual key is safe in keyStr
            fits_read_record(reinterpret_cast<fitsfile*>(fptr), i, key, &status);
            if(status)
              throw TFitsException(status);
            if (strncmp(key, "CONTINUE", 8) != 0) {
                // require both trailing '&' and CONTINUE to invoke long-string handling
                break;
            }
            std::string card = key;
            valueStr.erase(valueStr.size() - 2);
            std::size_t firstQuote = card.find('\'');
            if (firstQuote == std::string::npos) {
                std::cerr << "Invalid CONTINUE at header key "<<i<<": \""<<card<<"\"." <<std::endl;
                continue;
            }
            std::size_t lastQuote = card.find('\'', firstQuote + 1);
            if (lastQuote == std::string::npos) {
               std::cerr << "Invalid CONTINUE at header key "<<i<<": \""<<card<<"\"." <<std::endl;
               continue;
            }
            valueStr += card.substr(firstQuote + 1, lastQuote - firstQuote);
            std::size_t slash = card.find('/', lastQuote + 1);
            if (slash != std::string::npos) {
                commentStr += strip(card.substr(slash + 1));
            }
            ++i;
        }
        loadKey(l,keyStr, valueStr, commentStr,is_strip);
    }
}

template <typename T>
void TFits::writeKeyImpl(char const * key, T const & value, char const * comment) {
    fits_write_key(
        reinterpret_cast<fitsfile*>(fptr),
        FitsType<T>::CONSTANT,
        const_cast<char*>(key),
        const_cast<T *>(&value),
        const_cast<char*>(comment),
        &status
    );
}

template<>
void TFits::writeKeyImpl<float>(char const * key, float const & value, char const * comment) {
    writeKeyImpl(key, static_cast<double>(value), comment);
}


void TFits::writeKeyImpl(char const * key, std::string const & value, char const * comment) {
    if (strncmp(key, "COMMENT", 7) == 0) {
        fits_write_comment(
            reinterpret_cast<fitsfile*>(fptr),
            const_cast<char*>(value.c_str()),
            &status
        );
    } else if (strncmp(key, "HISTORY", 7) == 0) {
        fits_write_history(
            reinterpret_cast<fitsfile*>(fptr),
            const_cast<char*>(value.c_str()),
            &status
        );
    } else {
        fits_write_key_longstr(
            reinterpret_cast<fitsfile*>(fptr),
            const_cast<char*>(key),
            const_cast<char*>(value.c_str()),
            const_cast<char*>(comment),
            &status
        );
    }
}

void TFits::writeKeyImpl(char const * key, bool const & value, char const * comment) {
    int v = value;
    fits_write_key(
        reinterpret_cast<fitsfile*>(fptr),
        TLOGICAL,
        const_cast<char*>(key),
        &v,
        const_cast<char*>(comment),
        &status
    );
}

void TFits::writeKeyImpl(char const * key, double const & value, char const * comment) {
    std::string strValue = nonFiniteDoubleToString(value);
    if (!strValue.empty()) {
        writeKeyImpl(key, strValue, comment);
    } else {
        fits_write_key(
            reinterpret_cast<fitsfile*>(fptr),
            FitsType<double>::CONSTANT,
            const_cast<char*>(key),
            const_cast<double*>(&value),
            const_cast<char*>(comment),
            &status
            );
    }
}

void TFits::writeMetadata(const PropertyList & metadata, bool strip){
    using v_acc = PropertyList::value_acc;

    for(auto& tuple : metadata){
        if (strip && isKeyIgnored(std::get<v_acc::KEY>(tuple)))
            return;
        const std::string& key = std::get<v_acc::KEY>(tuple);
        const boost::any& val = std::get<v_acc::VAL>(tuple);
        const std::string& com = std::get<v_acc::COM>(tuple);

        const std::type_info& valueType = val.type();
        if (valueType == typeid(bool))
            writeKeyImpl(key.c_str(), boost::any_cast<bool>(val), com.empty()?nullptr:com.c_str());
        else if (valueType == typeid(char))
            writeKeyImpl(key.c_str(), boost::any_cast<char>(val), com.empty()?nullptr:com.c_str());
        else if (valueType == typeid(unsigned char))
            writeKeyImpl(key.c_str(), boost::any_cast<unsigned char>(val), com.empty()?nullptr:com.c_str());
        else if (valueType == typeid(short))
            writeKeyImpl(key.c_str(), boost::any_cast<short>(val), com.empty()?nullptr:com.c_str());
        else if (valueType == typeid(unsigned short))
            writeKeyImpl(key.c_str(), boost::any_cast<unsigned short>(val), com.empty()?nullptr:com.c_str());
        else if (valueType == typeid(int))
            writeKeyImpl(key.c_str(), boost::any_cast<int>(val), com.empty()?nullptr:com.c_str());
        else if (valueType == typeid(unsigned int))
            writeKeyImpl(key.c_str(), boost::any_cast<unsigned int>(val), com.empty()?nullptr:com.c_str());
        else if (valueType == typeid(long))
            writeKeyImpl(key.c_str(), boost::any_cast<long>(val), com.empty()?nullptr:com.c_str());
        else if (valueType == typeid(unsigned long))
            writeKeyImpl(key.c_str(), boost::any_cast<unsigned long>(val), com.empty()?nullptr:com.c_str());
        else if (valueType == typeid(long long))
            writeKeyImpl(key.c_str(), boost::any_cast<long long>(val), com.empty()?nullptr:com.c_str());
        else if (valueType == typeid(unsigned long long))
            writeKeyImpl(key.c_str(), boost::any_cast<unsigned long long>(val), com.empty()?nullptr:com.c_str());
        else if (valueType == typeid(float))
            writeKeyImpl(key.c_str(), boost::any_cast<float>(val), com.empty()?nullptr:com.c_str());
        else if (valueType == typeid(double))
            writeKeyImpl(key.c_str(), boost::any_cast<double>(val), com.empty()?nullptr:com.c_str());
/*        else if (valueType == typeid( std::complex<float> >))
            writeKeyImpl(key.c_str(), boost::any_cast<td::complex<float>>>(val), com.empty()?nullptr:com.c_str());
        else if (valueType == typeid( std::complex<double>> ))
            writeKeyImpl(key.c_str(), boost::any_cast<std::complex<double>>>(val), com.empty()?nullptr:com.c_str());
*/        else if (valueType == typeid(std::string))
            writeKeyImpl(key.c_str(), boost::any_cast<std::string>(val), com.empty()?nullptr:com.c_str());
        else
            std::cerr << "keyword type '"<< valueType.name()<<"' not suportded" << std::endl;
    }
}



///////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////     IMAGE HANDLING    ///////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
void TFits::createEmpty() {
    long naxes = 0;
    fits_create_img(reinterpret_cast<fitsfile*>(fptr), 8, 0, &naxes, &status);
}


template <typename PixelT>
void TFits::createImage(long x, long y){
    long naxes[] = {x,y};
    fits_create_img(
            reinterpret_cast<fitsfile*>(fptr),
            FitsBitPix<PixelT>::CONSTANT,
            2,
            naxes,
            &status);
}

template <typename PixelT>
CommonTools::Array<PixelT> TFits::readImage(){
    int dim = getImageDim();
    if(dim != 2){
        std::ostringstream err;
        err << "Bad number of dimensions for extension " << getHdu() << ": " << dim;
        throw std::runtime_error(err.str());
    }
    long naxes[2];
    fits_get_img_size(reinterpret_cast<fitsfile*>(fptr),2,naxes, &status);

    CommonTools::Array<PixelT> array(naxes[1],naxes[0]);

    fits_read_img (
            reinterpret_cast<fitsfile*>(fptr),
            FitsType<PixelT>::CONSTANT,
            1, naxes[1]*naxes[0],0,
            array.data(), 0,
            &status);

    return array;
}

template <typename PixelT>
void TFits::writeImage(const  CommonTools::Array<PixelT>& array){
    fits_write_img(
        reinterpret_cast<fitsfile*>(fptr),
        FitsType<PixelT>::CONSTANT,
        1, array.size(),
        const_cast<PixelT*>(array.data()),
        &status
    );
}

template <typename T>
bool TFits::checkImageType(){
    int bitpix = 0;
    fits_get_img_equivtype(reinterpret_cast<fitsfile*>(fptr), &bitpix, &status);
    return bitpix == FitsBitPix<T>::CONSTANT;
}
///////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////   UTILITIES   //////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////


// Strip leading and trailing single quotes and whitespace from a string.
std::string strip(std::string const & s) {
    if (s.empty()) return s;
    std::size_t i1 = s.find_first_not_of(" '");
    std::size_t i2 = s.find_last_not_of(" '");
    return s.substr(i1, (i1 == std::string::npos) ? 0 : 1 + i2 - i1);
}

/// Convert a special string to double when reading FITS keyword values
///
/// Returns zero if the provided string is not one of the recognised special
/// strings for doubles; otherwise, returns the mapped value.

double stringToNonFiniteDouble(std::string const& value)
{
    if (value == "NAN") {
        return std::numeric_limits<double>::quiet_NaN();
    }
    if (value == "+INFINITY") {
        return std::numeric_limits<double>::infinity();
    }
    if (value == "-INFINITY") {
        return -std::numeric_limits<double>::infinity();
    }
    return 0;
}

/// Convert a double to a special string for writing FITS keyword values
///
/// Non-finite values are written as special strings.  If the value is finite,
/// an empty string is returned.
std::string nonFiniteDoubleToString(double value)
{
    if (std::isfinite(value)) {
        return "";
    }
    if (std::isnan(value)) {
        return "NAN";
    }
    if (value < 0) {
        return "-INFINITY";
    }
    return "+INFINITY";
}

bool isKeyIgnored(std::string const & key) {
    return key == "SIMPLE" || key == "BITPIX" || key == "EXTEND" ||
        key == "GCOUNT" || key == "PCOUNT" || key == "XTENSION" || key == "TFIELDS" ||
        key == "BSCALE" || key == "BZERO" || key.compare(0, 5, "NAXIS") == 0; /*REMOVE ME!!!*/
        // ||  key == "HISTORY" || key == "COMMENT";
}

void loadKey(
    PropertyList & l,
    std::string const & key,
    std::string const & value,
    std::string const & comment,
    bool strip
) {
    static boost::regex const boolRegex("[tTfF]");
    static boost::regex const intRegex("[+-]?[0-9]+");
    static boost::regex const doubleRegex(
            "[+-]?([0-9]*\\.?[0-9]+|[0-9]+\\.?[0-9]*)([eE][+-]?[0-9]+)?");
    static boost::regex const fitsStringRegex("'(.*?) *'");
    boost::smatch matchStrings;

    if (strip && isKeyIgnored(key)) return;

    std::istringstream converter(value);
    if (boost::regex_match(value, boolRegex)) {
        // convert the string to an bool
        l.add(key, bool(value == "T" || value == "t"), comment);
    } else if (boost::regex_match(value, intRegex)) {
        // convert the string to an int
        long long val;
        converter >> val;
        if (val < (1LL << 31) && val > -(1LL << 31)) {
            l.add(key, static_cast<int>(val), comment);
        } else {
            l.add(key, val, comment);
        }
    } else if (boost::regex_match(value, doubleRegex)) {
        // convert the string to a double
        double val;
        converter >> val;
        l.add(key, val, comment);
    } else if (boost::regex_match(value, matchStrings, fitsStringRegex)) {
        std::string const str = matchStrings[1].str(); // strip off the enclosing single quotes
        double val = stringToNonFiniteDouble(str);
        if (val != 0.0) {
            l.add(key, val, comment);
        } else {
            l.add(key, str, comment);
        }
    } else if (key == "HISTORY" || key == "COMMENT") {
        l.add(key, comment, "");
    } else if (value.empty()) {
        // do nothing for empty values
    } else {
        std::cerr << "Could not parse header value for key '" << key << "': '"<<value <<std::endl;
    }
}


#define INSTANTIATE_IMAGE_OPS(r, data, T)                        \
    template void TFits::createImage<T>(long x, long y); \
    template void TFits::writeImage(const  CommonTools::Array<T>& array);\
    template CommonTools::Array<T> TFits::readImage(); \
    template bool TFits::checkImageType<T>();

#define IMAGE_TYPES                                                     \
    (unsigned char)(short)(unsigned short)(int)(unsigned int)(long)(unsigned long)(long long)(unsigned long long) \
    (float)(double)

BOOST_PP_SEQ_FOR_EACH(INSTANTIATE_IMAGE_OPS, _, IMAGE_TYPES)

}// Euclid

