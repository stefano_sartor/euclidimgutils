/*
 * Copyright (C) 2012-2020 Euclid Science Ground Segment
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include "EuclidImage/PropertyList.h"
#include <iostream>
#include <sstream>
#include <algorithm> 
#include <unordered_map>
#include <utility> 
#include <tuple>
#include <stdexcept>
#include <typeinfo>
#include <vector>
#include <boost/regex.hpp>


namespace Euclid{

CastException::CastException():_msg("possible precision loss in conversion."){
}

CastException::CastException(const std::string& msg){
  _msg = "possible precitions loss: " + msg;
}

const char* CastException::what() const noexcept{
  return _msg.c_str();
}

class PropertyList::Imp {
 public:
 Imp(std::shared_ptr<PropertyList::Imp> parent=std::shared_ptr<PropertyList::Imp>()):
  _parent(parent){}
  void set(const std::string& key, boost::any e, const std::string& comment="", bool inPlace=true);
  void add(const std::string& key, boost::any e, const std::string& comment="");
  std::vector<iterator>* get(const std::string& key); // nullptr if no such key

  iterator begin(){return _l.begin();}
  const_iterator begin()const{return _l.begin();}
  const_iterator cbegin()const{return _l.cbegin();}


  iterator end(){return _l.end();}
  const_iterator end()const{return _l.end();}
  const_iterator cend()const{return _l.cend();}


  std::list<value_type>& getList(){return _l;} ///DEBUG
  void print();///DEBUG
  
  const std::shared_ptr<Imp>& parent()const {return _parent;}

  std::shared_ptr<PropertyList::Imp> copy()const;

  bool hasKey(const std::string& key, bool parent = true) const;

  std::vector<std::string> keys() const;

 private:
  Imp* _find(const std::string& key); // nullptr if not such key
  void _add(const value_type& i);
//  void _replace(const value_type& i, bool inPlace);
  void _replace_all(const value_type& i, bool inPlace);
  void _replace_last(const value_type& i, bool inPlace);
  
  std::unordered_map<std::string,std::vector<iterator>> _m;
  std::list<value_type> _l;
  std::shared_ptr<Imp> _parent;

};


std::shared_ptr<PropertyList::Imp> PropertyList::Imp::copy()const{
    std::shared_ptr<Imp> c(new Imp());

    if(_parent)
        c->_parent = _parent->copy();

    for(const auto& v:*this){
        auto it = c->_l.insert(c->_l.end(),v);
        c->_m[std::get<value_acc::KEY>(v)].push_back(it);
    }
    return c;
}

bool PropertyList::Imp::hasKey(const std::string& key, bool parent) const {
  if(_m.find(key) != _m.end())
    return true;
  if (parent && _parent)
    return _parent->hasKey(key);
  return false;
}

std::vector<std::string> PropertyList::Imp::keys() const {
    std::vector<std::string> v;
    v.reserve(_l.size());

    for(auto& k : _l){
        v.push_back(std::get<PropertyList::KEY>(k));
    }
    return v;
}

std::vector<PropertyList::iterator>* PropertyList::Imp::get(const std::string& key){
  Imp* owner = _find(key);
  if(! owner)
    return nullptr;

  return &(owner->_m.at(key));
 }

PropertyList::Imp* PropertyList::Imp::_find(const std::string& key){
  if(_m.find(key) != _m.end())
    return this;
  if(_parent)
    return _parent->_find(key);
  else
    return nullptr;
}


void PropertyList::Imp::_add(const value_type& i){
  auto it = _l.insert(_l.end(),i);
  _m[std::get<value_acc::KEY>(i)].push_back(it);
}

void PropertyList::Imp::_replace_all(const value_type& i, bool inPlace){
    auto& v = _m.at(std::get<value_acc::KEY>(i)); // reference to vector
    for(auto& l_it: v){ //list iterator
        std::get<value_acc::VAL>(*l_it) = std::get<value_acc::VAL>(i);
        std::get<value_acc::COM>(*l_it) = std::get<value_acc::COM>(i);
        if(!inPlace){
          _l.splice(_l.end(),_l,l_it);
        }
    }
}

void PropertyList::Imp::_replace_last(const value_type& i, bool inPlace){
    auto& v = _m.at(std::get<value_acc::KEY>(i)); // reference to vector
    auto& l_it = v.back();
    std::get<value_acc::VAL>(*l_it) = std::get<value_acc::VAL>(i);
    std::get<value_acc::COM>(*l_it) = std::get<value_acc::COM>(i);
    if(!inPlace){
      _l.splice(_l.end(),_l,l_it);
    }
}

/*
void PropertyList::Imp::_replace(const value_type& i, bool inPlace){
  auto& it = _m.at(std::get<value_acc::KEY>(i));
  std::get<value_acc::VAL>(*it) = std::get<value_acc::VAL>(i);
  std::get<value_acc::COM>(*it) = std::get<value_acc::COM>(i);

  if(!inPlace){
    _l.splice(_l.end(),_l,it);
  }
}
*/

void PropertyList::Imp::set(
        const std::string& key, boost::any e,const std::string& comment, bool inPlace){
  Imp* owner= _find(key);
  value_type i = std::make_tuple(key,e,comment);
  if(! owner){
    _add(i);
    return;
  }else{
      while(owner){ // replace value in ancestors lists
          owner->_replace_all(i,inPlace);
          if(owner->_parent)
              owner = owner->_parent->_find(key);
          else
              owner = nullptr;
      }
  }
}

void PropertyList::Imp::add(const std::string& key, boost::any e, const std::string& comment){
    _add(std::make_tuple(key,e,comment));
}

void PropertyList::Imp::print(){
  if(_parent){
    _parent->print();
    std::cout << "*   *   *   *   *" << std::endl;
  }
  for(auto& i : _l){
    std::cout <<"key:"<<std::get<0>(i) << " value:";
    const std::type_info& t = std::get<1>(i).type();
    if(t == typeid(char)) std::cout << boost::any_cast<char>(std::get<1>(i));
    else if(t == typeid(bool))std::cout << std::boolalpha <<boost::any_cast<bool>(std::get<1>(i));
    else if(t == typeid(unsigned char))std::cout << boost::any_cast<unsigned char>(std::get<1>(i));
    else if(t == typeid(short))std::cout << boost::any_cast<short>(std::get<1>(i));
    else if(t == typeid(unsigned short))std::cout << boost::any_cast<unsigned short>(std::get<1>(i));
    else if(t == typeid(int))std::cout << boost::any_cast<int>(std::get<1>(i));
    else if(t == typeid(unsigned int))std::cout << boost::any_cast<unsigned int>(std::get<1>(i));
    else if(t == typeid(long long))std::cout << boost::any_cast<long long>(std::get<1>(i));
    else if(t == typeid(unsigned long long))std::cout << boost::any_cast<unsigned long long>(std::get<1>(i));
    else if(t == typeid(float))std::cout << boost::any_cast<float>(std::get<1>(i));
    else if(t == typeid(double))std::cout << boost::any_cast<double>(std::get<1>(i));
    else if(t == typeid(std::string))std::cout << boost::any_cast<std::string>(std::get<1>(i));
    else std::cout << "unknown type:" << std::get<1>(i).type().name();
    std::cout <<"  comment:"<<std::get<2>(i) << std::endl;
  }
}

PropertyList::PropertyList() : _impl(new Imp()){}

PropertyList::PropertyList(std::shared_ptr<Imp> parent) : _impl(new Imp(parent)){}

PropertyList PropertyList::getParent() const {
    if(_impl->parent())
        return PropertyList(_impl->parent());
    else
        throw std::runtime_error("PropertyList has no parent");
}

bool PropertyList::hasParent()const{
    return bool(_impl->parent());
}

PropertyList::iterator PropertyList::begin(){return _impl->begin();}
PropertyList::const_iterator PropertyList::begin()const{return _impl->begin();}
PropertyList::const_iterator PropertyList::cbegin()const{return _impl->cbegin();}


PropertyList::iterator PropertyList::end(){return _impl->end();}
PropertyList::const_iterator PropertyList::end()const{return _impl->end();}
PropertyList::const_iterator PropertyList::cend()const{return _impl->cend();}

template<typename T>
void PropertyList::set(
        const std::string& key,  const T& e, const std::string& comment, bool inPlace){
  _impl->set(key,e,comment,inPlace);
}

template<typename T>
void PropertyList::add(const std::string& key,  const T& e, const std::string& comment){
   _impl->add(key,e,comment); 
}


template<typename T>
T PropertyList::get(const std::string& key){
  std::vector<PropertyList::iterator>* v = _impl->get(key);
  if(!v)
    throw std::out_of_range("keyword "+key+" not present.");
  else
    return boost::any_cast<T>(std::get<value_acc::VAL>(*v->back()));
}

template<typename T>
T PropertyList::get(const std::string& key, const T& defaultValue){
  std::vector<PropertyList::iterator>* v = _impl->get(key);
  if(!v)
    return defaultValue;
  else
    return boost::any_cast<T>(std::get<value_acc::VAL>(*v->back()));
}


PropertyList PropertyList::makeChild(){
  return PropertyList(_impl);
}

void PropertyList::setParent(const PropertyList& p){
  _impl = p._impl;
}

void PropertyList::print(){_impl->print();}

void PropertyList::set(
        const std::string& key, const char* e, const std::string& comment, bool inPlace){
  set(key,std::string(e),comment,inPlace);
}

void PropertyList::add(const std::string& key, const char* e, const std::string& comment){
  add(key,std::string(e),comment);
}

bool PropertyList::getAsBool(std::string const& key){
    return get<bool>(key);
}

int PropertyList::getAsInt(std::string const& key) {
  auto* v= _impl->get(key);
  
  if(!v)
    throw std::out_of_range("keyword "+key+" not found.");

  boost::any& e = std::get<value_acc::VAL>(*v->back());

  std::type_info const& t = e.type();
  if (t == typeid(bool))          return boost::any_cast<bool>(e);
  if (t == typeid(char))          return boost::any_cast<char>(e);
  if (t == typeid(signed char))   return boost::any_cast<signed char>(e);
  if (t == typeid(unsigned char)) return boost::any_cast<unsigned char>(e);
  if (t == typeid(short))         return boost::any_cast<short>(e);
  if (t == typeid(unsigned short))return boost::any_cast<unsigned short>(e);
  
  return boost::any_cast<int>(e);
}

int64_t PropertyList::getAsInt64(std::string const& key) {
    auto* v= _impl->get(key);

    if(!v)
        throw std::out_of_range("keyword "+key+" not found.");

    boost::any& e = std::get<value_acc::VAL>(*v->back());

    std::type_info const& t = e.type();
    if (t == typeid(bool))           return boost::any_cast<bool>(e);
    if (t == typeid(char))           return boost::any_cast<char>(e);
    if (t == typeid(signed char))    return boost::any_cast<signed char>(e);
    if (t == typeid(unsigned char))  return boost::any_cast<unsigned char>(e);
    if (t == typeid(short))          return boost::any_cast<short>(e);
    if (t == typeid(unsigned short)) return boost::any_cast<unsigned short>(e);
    if (t == typeid(int))            return boost::any_cast<int>(e);
    if (t == typeid(unsigned int))   return boost::any_cast<unsigned int>(e);
    if (t == typeid(long))           return boost::any_cast<long>(e);
    if (t == typeid(long long))      return boost::any_cast<long long>(e);

    return boost::any_cast<int64_t>(e);
}

double PropertyList::getAsDouble(std::string const& key) {
    auto* v= _impl->get(key);

    if(!v)
        throw std::out_of_range("keyword "+key+" not found.");

    boost::any& e = std::get<value_acc::VAL>(*v->back());

    std::type_info const& t = e.type();
    if (t == typeid(bool))               return boost::any_cast<bool>(e);
    if (t == typeid(char))               return boost::any_cast<char>(e);
    if (t == typeid(signed char))        return boost::any_cast<signed char>(e);
    if (t == typeid(unsigned char))      return boost::any_cast<unsigned char>(e);
    if (t == typeid(short))              return boost::any_cast<short>(e);
    if (t == typeid(unsigned short))     return boost::any_cast<unsigned short>(e);
    if (t == typeid(int))                return boost::any_cast<int>(e);
    if (t == typeid(unsigned int))       return boost::any_cast<unsigned int>(e);
    if (t == typeid(long))               return boost::any_cast<long>(e);
    if (t == typeid(unsigned long))      return boost::any_cast<unsigned long>(e);
    if (t == typeid(long long))          return boost::any_cast<long long>(e);
    if (t == typeid(unsigned long long)) return boost::any_cast<unsigned long long>(e);
    if (t == typeid(float))              return boost::any_cast<float>(e);

    // not reached
    return boost::any_cast<double>(e);
}


const std::type_info& PropertyList::typeOf(std::string const& key) const {
  auto* v= _impl->get(key);

  if(!v)
      throw std::out_of_range("keyword "+key+" not found.");

  boost::any& e = std::get<value_acc::VAL>(*v->back());
  return e.type();
}


PropertyList PropertyList::copy()const{
    return PropertyList(_impl->copy());
}


bool PropertyList::hasKey(const std::string& key, bool parent) const {
    return _impl->hasKey(key, parent);
}

std::vector<std::string> PropertyList::keys() const {
    return _impl->keys();
}

#define INSTANTIALTE_PROPERTYLIST(T) \
  template void PropertyList::set(const std::string&, const T&, const std::string&, bool);\
  template void PropertyList::add(const std::string&, const T&, const std::string&);\
  template T PropertyList::get(const std::string&);\
  template T PropertyList::get(const std::string&,const T&);

INSTANTIALTE_PROPERTYLIST(bool)
INSTANTIALTE_PROPERTYLIST(char)
INSTANTIALTE_PROPERTYLIST(unsigned char)
INSTANTIALTE_PROPERTYLIST(short)
INSTANTIALTE_PROPERTYLIST(unsigned short)
INSTANTIALTE_PROPERTYLIST(int)
INSTANTIALTE_PROPERTYLIST(unsigned int)
INSTANTIALTE_PROPERTYLIST(long)
INSTANTIALTE_PROPERTYLIST(unsigned long)
INSTANTIALTE_PROPERTYLIST(long long)
INSTANTIALTE_PROPERTYLIST(unsigned long long)
INSTANTIALTE_PROPERTYLIST(float)
INSTANTIALTE_PROPERTYLIST(double)
INSTANTIALTE_PROPERTYLIST(std::string)

}// Euclid
