/*
 * Copyright (C) 2012-2020 Euclid Science Ground Segment
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include "DetectorExposure.h"
#include "TFits.h"

#include <stdexcept>
#include <boost/format.hpp>

namespace Euclid {


template<typename ImageT, typename RootMeanSqT, typename MaskT>
DetectorExposure<ImageT, RootMeanSqT, MaskT>::DetectorExposure(size_type width, size_type height,
                                                               const MaskPlaneDict& planeDict):
  m_image(width, height),
  m_rms(width, height),
  m_mask(width, height, planeDict)
{}


template<typename ImageT, typename RootMeanSqT, typename MaskT>
DetectorExposure<ImageT, RootMeanSqT, MaskT>::DetectorExposure(const ImageT& image, const RootMeanSqT& rms, const MaskT& mask):
  m_image(image),
  m_rms(rms),
  m_mask(mask)
{
  conformSizes();
}


template<typename ImageT, typename RootMeanSqT, typename MaskT>
DetectorExposure<ImageT, RootMeanSqT, MaskT>::DetectorExposure(const DetectorExposure& other, bool deepCopy):
  m_image(other.m_image, deepCopy),
  m_rms(other.m_rms, deepCopy),
  m_mask(other.m_mask, deepCopy),
  m_metadata((deepCopy) ? other.m_metadata.copy() : other.m_metadata)
{}


template<typename ImageT, typename RootMeanSqT, typename MaskT>
DetectorExposure<ImageT, RootMeanSqT, MaskT>
DetectorExposure<ImageT, RootMeanSqT, MaskT>::readFits(std::string fileName, int hdu, bool primaryHdr)
{
  TFits inFits(fileName, "r");
  int hduCount = inFits.countHdus();

  if (hdu > hduCount)
    throw std::runtime_error(str(boost::format("Invalid HDU number: %d. File has %d HDUs") %
        hdu % hduCount));

  PropertyList metadata;

  if (hdu != 1 && primaryHdr) {
    inFits.setHdu(1);
    inFits.readMetadata(metadata);
    metadata = metadata.makeChild();
  }

  inFits.setHdu(hdu);
  inFits.readMetadata(metadata);

  ImageT image(inFits.readImage<typename ImageT::value_type>());

  ++hdu;

  RootMeanSqT rms(0,0);

  if (hdu < hduCount)
  {
    inFits.setHdu(hdu);
    rms = RootMeanSqT(inFits.readImage<typename RootMeanSqT::value_type>());
  }

  ++hdu;

  MaskT mask(0,0);

  if (hdu < hduCount)
  {
    inFits.setHdu(hdu);
    mask = MaskT(inFits.readImage<typename MaskT::value_type>());
  }


  auto result = DetectorExposure<ImageT, RootMeanSqT, MaskT>(image, rms, mask);
  result.setMetadata(metadata);
  return result;

}


template<typename ImageT, typename RootMeanSqT, typename MaskT>
void DetectorExposure<ImageT, RootMeanSqT, MaskT>::writeFits(std::string fileName) const
{
  TFits outputFits(fileName, "w");

  outputFits.createEmpty();

  if (m_metadata.hasParent())
    outputFits.writeMetadata(m_metadata.getParent());


  // TODO: Setting the extension name is a little convoluted. Some improvement necessary
  const std::string extname = "EXTNAME";
  PropertyList extname_property;

  outputFits.createImage<typename ImageT::value_type>(m_image.getWidth(), m_image.getHeight());
  outputFits.writeImage<typename ImageT::value_type>(m_image.getArray());
  if (!m_metadata.hasKey(extname)){
    extname_property.set(extname, "SCI");
    outputFits.writeMetadata(extname_property);
  }
  outputFits.writeMetadata(m_metadata);

  extname_property.set(extname, "ERR");
  outputFits.createImage<typename RootMeanSqT::value_type>(m_rms.getWidth(), m_rms.getHeight());
  outputFits.writeImage<typename RootMeanSqT::value_type>(m_rms.getArray());
  outputFits.writeMetadata(extname_property);

  extname_property.set(extname,"DQ");
  // TODO: save also the bit-plane dictionary
  outputFits.createImage<typename MaskT::value_type>(m_mask.getWidth(), m_mask.getHeight());
  outputFits.writeImage<typename MaskT::value_type>(m_mask.getArray());
  outputFits.writeMetadata(extname_property);

}


template<typename ImageT, typename RootMeanSqT, typename MaskT>
void DetectorExposure<ImageT, RootMeanSqT, MaskT>::conformSizes()
{
  if (m_rms.getWidth() == 0 || m_rms.getHeight() == 0)
  {
    m_rms = RootMeanSqT(m_image.getWidth(), m_image.getHeight());
  }
  else if (m_rms.getWidth() != m_image.getWidth() || m_rms.getHeight() != m_image.getHeight())
    throw std::length_error(std::length_error(str(boost::format("Image and RMS are of different size: %dx%d v %dv%d") %
        m_image.getWidth() % m_image.getHeight() %
        m_rms.getWidth() % m_rms.getHeight())));
  if (m_mask.getWidth() == 0 || m_mask.getHeight() == 0)
  {
    m_mask = MaskT(m_image.getWidth(), m_image.getHeight());
  }
  else if (m_mask.getWidth() != m_image.getWidth() || m_mask.getHeight() != m_image.getHeight())
    throw std::length_error(std::length_error(str(boost::format("Image and RMS are of different size: %dx%d v %dv%d") %
        m_image.getWidth() % m_image.getHeight() %
        m_mask.getWidth() % m_mask.getHeight())));
}

#define INSTANTIATE(PixelT, RmsPixelT, MaskPixelT) \
  template class DetectorExposure<Image<PixelT>, Image<RmsPixelT>, Mask<MaskPixelT>>

INSTANTIATE(float, float, uint16_t);

}
