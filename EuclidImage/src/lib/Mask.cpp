/*
 * Copyright (C) 2012-2020 Euclid Science Ground Segment
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include "Mask.h"

#include <algorithm>
#include <functional>
#include <stdexcept>

#include "boost/format.hpp"

namespace Euclid {

using MaskPlaneDict = std::map<std::string, int>;

template<typename MaskPixelT>
Mask<MaskPixelT>::Mask(size_type width, size_type height,
		       const MaskPlaneDict& planeDefs):
  super(width,height), m_planeDefs(planeDefs)
{
  *this = 0x0;
}
  
template<typename MaskPixelT>
Mask<MaskPixelT>::Mask(size_type width, size_type height,
		       MaskPixelT initialValue,
		       const MaskPlaneDict& planeDefs):
  super(width,height), m_planeDefs(planeDefs)
{
  super::operator=(initialValue);
}


template<typename MaskPixelT>
Mask<MaskPixelT>::Mask(const Mask& other, bool deepCopy):
  super(other, deepCopy), m_planeDefs(other.m_planeDefs)
{ }


template<typename MaskPixelT>
Mask<MaskPixelT>::Mask(const Array& array, bool deepCopy):
  super(array, deepCopy)
{ }


template<typename MaskPixelT>
Mask<MaskPixelT>& Mask<MaskPixelT>::operator=(const MaskPixelT& value)
{
  super::operator=(value);
  return *this;
}


template<typename MaskPixelT>
Mask<MaskPixelT>& Mask<MaskPixelT>::operator=(const Mask& other)
{
  super::operator=(other);
  m_planeDefs = other.m_planeDefs;
  return *this;
}


template<typename MaskPixelT>
Mask<MaskPixelT>& Mask<MaskPixelT>::operator|=(const MaskPixelT& rhs)
{
  std::transform(super::cbegin(), super::cend(), super::begin(),
                 std::bind2nd(std::bit_or<MaskPixelT>(), rhs));
  return *this;
}


template<typename MaskPixelT>
Mask<MaskPixelT>& Mask<MaskPixelT>::operator|=(const Mask<MaskPixelT>& rhs)
{
  checkDimensions(rhs);
  std::transform(rhs.cbegin(), rhs.cend(), super::cbegin(), super::begin(),
                 std::bit_or<MaskPixelT>());
  return *this;
}


template<typename MaskPixelT>
Mask<MaskPixelT>& Mask<MaskPixelT>::operator&=(const MaskPixelT& rhs)
{
  std::transform(super::cbegin(), super::cend(), super::begin(),
                 std::bind2nd(std::bit_and<MaskPixelT>(), rhs));
  return *this;
}


template<typename MaskPixelT>
Mask<MaskPixelT>& Mask<MaskPixelT>::operator&=(const Mask<MaskPixelT>& rhs)
{
  checkDimensions(rhs);
  std::transform(rhs.cbegin(), rhs.cend(), super::cbegin(), super::begin(),
                 std::bit_and<MaskPixelT>());
  return *this;
}


template<typename MaskPixelT>
Mask<MaskPixelT>& Mask<MaskPixelT>::operator^=(const MaskPixelT& rhs)
{
  std::transform(super::cbegin(), super::cend(), super::begin(),
                 std::bind2nd(std::bit_xor<MaskPixelT>(), rhs));
  return *this;
}


template<typename MaskPixelT>
Mask<MaskPixelT>& Mask<MaskPixelT>::operator^=(const Mask<MaskPixelT>& rhs)
{
  checkDimensions(rhs);
  std::transform(rhs.cbegin(), rhs.cend(), super::cbegin(), super::begin(),
                 std::bit_xor<MaskPixelT>());
  return *this;
}


template<typename MaskPixelT>
bool Mask<MaskPixelT>::operator()(size_type x, size_type y,
				   const std::string& plane) const
{
  return !!(super::operator()(x,y) & Mask<MaskPixelT>::getBitMask(m_planeDefs.at(plane)));
}


// same as before but check dimension first
template<typename MaskPixelT>
bool Mask<MaskPixelT>::at(size_type x, size_type y, 
			     const std::string& plane) const
{
  return !!(super::at(x,y) & getBitMask(m_planeDefs.at(plane)));
}


template<typename MaskPixelT>
const MaskPlaneDict& Mask<MaskPixelT>::getMaskPlaneDict() const
{
  return m_planeDefs;
}


template<typename MaskPixelT>
int Mask<MaskPixelT>::getMaskPlane(const std::string& planeName) const
{
  return m_planeDefs.at(planeName);
}


template<typename MaskPixelT>
MaskPixelT Mask<MaskPixelT>::getPlaneBitMask(const std::string& planeName) const
{
  return getBitMask(getMaskPlane(planeName));
}


template<typename MaskPixelT>
void Mask<MaskPixelT>::setMaskPlaneDict(const MaskPlaneDict& planeDefs)
{
  m_planeDefs = planeDefs;
}


template<typename MaskPixelT>
void Mask<MaskPixelT>::checkDimensions(const Mask<MaskPixelT>& other) const
{
  if (this->getWidth() != other.getWidth() || this->getHeight() != other.getHeight())
    throw std::length_error(str(boost::format("Masks are of different size: %dx%d v %dv%d") %
                                          this->getWidth() % this->getHeight() %
                                          other.getWidth() % other.getHeight()));
}

#define INSTANTIATE(T) \
  template class Mask<T>


INSTANTIATE(unsigned char);
INSTANTIATE(unsigned short);
INSTANTIATE(unsigned int);
INSTANTIATE(unsigned long);
INSTANTIATE(unsigned long long);

}
